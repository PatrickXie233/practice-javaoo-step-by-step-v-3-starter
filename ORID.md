**O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?**

1. codeReview：小组内相互进行codeReview，与组员沟通共享项目进度，可以帮助我们提高代码质量，解决技术问题，更新项目进度
2. codeReview实践：在codeReview实践中，我在查看他人的代码后发现我自己代码中的很多问题，比如可维护性不行，没用面向对象设计；在班内codeReview分享中，我学习到许多改进我代码质量的方法和思路，如git频繁提交，命名规范，函数拆解清晰等
3. Java Stream：主要学习了Stream的使用方法，包括filter方法，map，reduce等
4. 在OO学习设计中实践了OO的三大特性：封装，继承，多态

**R (Reflective): Please use one word to express your feelings about today's class.**

丰富的实践

**I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?**

在作业step7的设计模式还不是很了解

**D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?**

在今天的作业中对于面向对象设计还不是很熟练，需要不断的练习


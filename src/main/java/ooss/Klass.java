package ooss;

import java.util.ArrayList;
import java.util.Objects;

public class Klass {
    private int number;
    private Student leader;
    private ArrayList<Teacher> teacherList = new ArrayList<>();
    private ArrayList<Student> studentList = new ArrayList<>();

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }
    public void attach(Student student){
        this.studentList.add(student);
    }
    public void attach(Teacher teacher){
        this.teacherList.add(teacher);
    }

    public void assignLeader(Student leader){
        if (leader.getKlass()==null){
            System.out.println("It is not one of us.");
        }

        this.leader = leader;
        this.teacherSay();
        this.studentSay();
    }
    public void teacherSay(){
        this.teacherList.forEach(teacher -> teacher.sayWhenAssignLeader(this.leader,this.getNumber()));
    }
    public void studentSay(){
        this.studentList.forEach(student -> student.sayWhenAssignLeader(this.leader,this.getNumber()));
    }
    public boolean isLeader(Student student){
        return this.leader.equals(student);
    }
}

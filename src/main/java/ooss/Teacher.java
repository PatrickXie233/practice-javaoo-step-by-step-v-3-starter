package ooss;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person {
    private ArrayList<Klass> klassList = new ArrayList<>();
    public Teacher(int id, String name, int age) {
        super(id, name, age);
    }
    @Override
    public String introduce(){
        String klass_str = "";
        for (int i = 0; i < this.klassList.size(); i++) {
            klass_str += this.klassList.get(i).getNumber();
            if (i!=this.klassList.size()-1){
                klass_str += ", ";
            }else {
                klass_str += ".";
            }
        }
        return "My name is " + getName() + ". I am " + getAge() + " years old." + " I am a teacher. " + "I teach Class " + klass_str;
    }
    public void assignTo(Klass klass){
        this.klassList.add(klass);
    }
    public boolean belongsTo(Klass klass){
        if (this.klassList==null){
            return false;
        }
        if (this.klassList.contains(klass)){
            return true;
        }else {
            return false;
        }
    }
    public boolean isTeaching(Student student){
        return this.klassList.contains(student.getKlass());
    }
    public void sayWhenAssignLeader(Student student , int classNum){
        System.out.println("I am "
                + getName()
                + ", teacher of Class "
                + classNum
                + ". I know "
                + student.getName()
                + " become Leader.");
    }
}

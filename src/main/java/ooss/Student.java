package ooss;

public class Student extends Person {
    private Klass klass;

    public Student(int id, String name, int age) {
        super(id, name, age);
    }
    public void join(Klass klass){
        this.klass = klass;
    }
    public boolean isIn(Klass klass){
        if (this.klass==null){
            return false;
        }
        return this.klass.equals(klass);
    }

    public Klass getKlass() {
        return this.klass;
    }

    @Override
    public String introduce(){
        String last_str = "";
        if (klass.isLeader(this)){
            last_str = " I am the leader of class " + klass.getNumber() + ".";
        }else {
            last_str = ".";
        }
        return "My name is " + getName() + ". I am " + getAge() + " years old." + " I am a student." + last_str;
    }
    public void sayWhenAssignLeader(Student student,int classNum){
        System.out.println("I am " + getName() + ", student of Class " + classNum + ". I know " + student.getName()  + " become Leader.");
    }
}
